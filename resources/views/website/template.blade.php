<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>GGL</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('global_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('global_assets/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('global_assets/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('global_assets/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="{{asset('global_assets/assets/img/favicon.png')}}">
	<link href="{{asset('global_assets/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('global_assets/assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('global_assets/assets/css/mystyle.css')}}" rel="stylesheet" type="text/css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('styles')
</head>
<body>
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="{{route('index')}}" class="d-inline-block">
				<img src="{{asset('global_assets/assets/img/logo-ggl.svg')}}" class="img-logo"  alt="">
			</a>
		</div>
		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="page-content">
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<div class="sidebar-content">
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="#"><img src="{{asset('global_assets/images/placeholders/placeholder.jpg')}}" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>
							<div class="media-body">
								<div class="media-title font-weight-semibold">GGL</div>
								<div class="font-size-xs opacity-50">
                  Admin
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{route('index')}}" class="nav-link {{ request()->routeIs('index') ? 'active' : '' }}">
								<i class="icon-book"></i>
								<span>
									Barang
								</span>
							</a>
						</li>
            <li class="nav-item">
							<a href="{{route('transaksi.index')}}" class="nav-link {{ request()->routeIs('transaksi*') ? 'active' : '' }}">
								<i class="icon-coin-dollar"></i>
								<span>
									Transaki
								</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
    @yield('content')
	</div>
	<script src="{{asset('global_assets/js/main/jquery.min.js')}}"></script>
	<script src="{{asset('global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<script src="{{asset('global_assets/assets/js/app.js')}}"></script>
  <script src="{{asset('global_assets/assets/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{asset('global_assets/js/toastmessage.js') }}"></script>
	<script src="{{asset('global_assets/js/deleteit.js') }}"></script>
  @yield('scripts')
</body>
</html>
