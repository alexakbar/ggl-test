@extends('website.template')
@section('styles')
  <link href="{{ asset('global_assets/assets/plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">{{$type == 'create' ? "Tambah" : "Edit"}} Barang</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title"><b>Form Barang</b></h5>
				</div>
        <div class="card-body">
          <form class="kt-form" id="form-submit" data-action="{{($type == 'create') ? route('barang.created') : route('barang.update',$data->id)}}" enctype="multipart/form-data" autocomplete="off">
            {{csrf_field()}}
            <div class="form-group">
                <label>Gambar Barang</label>
                <input type="file" class="dropify" name="image" data-height="300" data-default-file="{{$type == 'create' ? '' : asset($data->gambar_barang)}}" />
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Nama Barang</label>
              <div class="col-lg-12">
                <input type="text" name="nama_barang" placeholder="masukan nama barang" value="{{$type == 'create' ? '' : $data->nama_barang}}" class="form-control">
              </div>
            </div>
            <button type="submit" class="btn btn-success float-right"><i class="icon-paperplane mr-2"></i> {{$type == 'create' ? "BUAT" : "SIMPAN"}}</button>
          </form>
        </div>
			</div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('global_assets/js/form/form-submit.js') }}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/spin.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/ladda.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/components_buttons.js')}}"></script>
<script src="{{ asset('global_assets/assets/plugins/fileuploads/js/dropify.min.js') }}"></script>
<script type="text/javascript">
  $('.dropify').dropify({
      messages: {
          'default': 'Drag and drop a file here or click',
          'replace': 'Drag and drop or click to replace',
          'remove': 'Remove',
          'error': 'Ooops, something wrong appended.'
      },
      error: {
          'fileSize': 'The file size is too big (1M max).'
      }
  });
</script>
@endsection
