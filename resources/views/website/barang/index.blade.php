@extends('website.template')
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Barang</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title">Daftar Barang</h5>
          <a href="{{route('barang.create')}}"><button type="button" class="btn btn-light"><i class="icon-plus3 mr-2"></i> Tambah Barang</button></a>
				</div>
				<table class="table datatable-barang">
					<thead>
						<tr>
							<th>No</th>
              <th>ID Barang</th>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
              <th class="width-10-percent">Stok</th>
							<th class="width-25-percent">Aksi</th>
						</tr>
					</thead>
					<tbody>
            <?php foreach ($barangs as $key => $barang): ?>
						<tr>
              <td>{{$key+1}}</td>
              <td>{{$barang->id}}</td>
							<td>{{$barang->kode_barang}}</td>
              <td><a href="{{route('barang.show',$barang->id)}}">{{$barang->nama_barang}}</a></td>
              <?php
              $stokIn = App\Entities\Stok::where('id_barang',$barang->id)->where('jenis_stok','in')->get()->sum('total_barang');
              $stokOut = App\Entities\Stok::where('id_barang',$barang->id)->where('jenis_stok','out')->get()->sum('total_barang');
              $jumlahTransaksiPembelian = App\Entities\Transaksi::where('id_barang',$barang->id)->where('jenis_transaksi','pembelian')->get()->sum('total_barang');
              $jumlahTransaksiPenjualan = App\Entities\Transaksi::where('id_barang',$barang->id)->where('jenis_transaksi','penjualan')->get()->sum('total_barang');
              $realStok = ($stokIn + $jumlahTransaksiPembelian) - ($jumlahTransaksiPenjualan + $stokOut);
               ?>
              <td>{{$realStok}}</td>
							<td>
                <a href="{{route('barang.stok.create',$barang->id)}}"><button type="button" class="btn btn-light"><i class="icon-plus3 mr-2"></i> Tambah stok</button></a>
                <a href="{{route('barang.edit',$barang->id)}}"><button type="button" class="btn btn-info"><i class="icon-pencil3 mr-2"></i> Edit</button></a>
                <button type="button" onclick="deleteIt(this)" data-action="{{route('barang.destroy',$barang->id)}}" class="btn btn-danger"><i class="icon-trash-alt mr-2"></i> Hapus</button>
              </td>
						</tr>
            <?php endforeach; ?>
					</tbody>
				</table>
			</div>

      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title">Daftar Stok</h5>
				</div>
				<table class="table datatable-barang">
					<thead>
						<tr>
							<th>No</th>
              <th>Nama Barang</th>
							<th>Type</th>
              <th class="width-10-percent">Stok</th>
							<th class="width-25-percent">Aksi</th>
						</tr>
					</thead>
					<tbody>
            <?php foreach ($stoks as $key => $stok): ?>
						<tr>
              <td>{{$key+1}}</td>
							<td>{{$stok->barang->nama_barang}}</td>
              <?php if ($stok->jenis_stok == "in"): ?>
                <td><span class="badge bg-success ml-md-3 mr-md-auto">In</span></td>
              <?php else: ?>
                <td><span class="badge bg-danger ml-md-3 mr-md-auto">Out</span></td>
              <?php endif; ?>
              <td>{{$stok->total_barang}}</td>
							<td>
                <a href="{{route('barang.stok.edit',$stok->id)}}"><button type="button" class="btn btn-info"><i class="icon-pencil3 mr-2"></i> Edit</button></a>
                <button type="button" onclick="deleteIt(this)" data-action="{{route('barang.stok.destroy',$stok->id)}}" class="btn btn-danger"><i class="icon-trash-alt mr-2"></i> Hapus</button>
              </td>
						</tr>
            <?php endforeach; ?>
					</tbody>
				</table>
			</div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/spin.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/ladda.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/components_buttons.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
