@extends('website.template')
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Transaksi</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title">Daftar Transaksi</h5>
          <a href="{{route('transaksi.create')}}"><button type="button" class="btn btn-light"><i class="icon-plus3 mr-2"></i> Tambah Transaksi</button></a>
				</div>
				<table class="table datatable-transaksi">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Transaksi</th>
              <th>Nama Barang</th>
							<th>Jenis Transaksi</th>
              <th class="width-10-percent">Jumlah barang</th>
							<th class="width-25-percent">Aksi</th>
						</tr>
					</thead>
					<tbody>
            <?php foreach ($transaksis as $key => $transaksi): ?>
						<tr>
              <td>{{$key+1}}</td>
							<td>{{$transaksi->kode_transaksi}}</td>
              <td>{{$transaksi->barang->nama_barang}}</td>
              <?php if ($transaksi->jenis_transaksi == "pembelian"): ?>
                <td><span class="badge bg-success ml-md-3 mr-md-auto">Pembelian</span></td>
              <?php else: ?>
                <td><span class="badge bg-danger ml-md-3 mr-md-auto">Penjualan</span></td>
              <?php endif; ?>
              <td>{{$transaksi->total_barang}}</td>
							<td>
                <a href="{{route('transaksi.edit',$transaksi->id)}}"><button type="button" class="btn btn-info"><i class="icon-pencil3 mr-2"></i> Edit</button></a>
                <button type="button" onclick="deleteIt(this)" data-action="{{route('transaksi.destroy',$transaksi->id)}}" class="btn btn-danger"><i class="icon-trash-alt mr-2"></i> Hapus</button>
              </td>
						</tr>
            <?php endforeach; ?>
					</tbody>
				</table>
			</div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/spin.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/ladda.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/components_buttons.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
