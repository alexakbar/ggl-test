@extends('website.template')
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Detail Barang</span></h4>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title"><b>Form Barang</b></h5>
				</div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <img src="{{asset($data->gambar_barang)}}" class="width-100-percent"  alt="">
            </div>
            <div class="col-md-8">
              <h3><b>Kode Barang</b></h3>
              <p class="f-s15">{{$data->kode_barang}}</p>
              <h3><b>Nama Barang</b></h3>
              <p class="f-s15">{{$data->nama_barang}}</p>
            </div>
          </div>
          <a href="{{route('index')}}"><button type="button" class="btn btn-success float-right"><i class="icon-arrow-left7 mr-2"></i> BACK</button></a>
        </div>
			</div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
