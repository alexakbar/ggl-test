@extends('website.template')
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">{{$type == 'create' ? "Tambah" : "Edit"}} Transaksi</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title"><b>Form Transaksi</b></h5>
				</div>
        <div class="card-body">
          <form class="kt-form" id="form-submit" data-action="{{($type == 'create') ? route('transaksi.created') : route('transaksi.update',$data->id)}}" enctype="multipart/form-data" autocomplete="off">
            {{csrf_field()}}
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Pilih Barang</label>
              <div class="col-lg-12">
                <select class="form-control select-search" name="id_barang" {{$type == 'create' ? '' : 'disabled'}} data-fouc>
                  <?php foreach ($barangs as $key => $barang): ?>
                    <option value="{{$barang->id}}">{{$barang->nama_barang}}</option>
                  <?php endforeach; ?>
									</select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Jumlah Barang </label>
              <div class="col-lg-12">
                <input type="number" name="total_barang" placeholder="masukan jumlah barang" value="{{$type == 'create' ? '' : $data->total_barang}}" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Pilih Type Transaki</label>
              <div class="col-lg-12">
                <select class="form-control select" name="jenis_transaksi" data-fouc>
                  <option value="penjualan"{{@$data->jenis_transaksi == 'penjualan' ? 'selected' : ''}}>Penjualan</option>
                  <option value="pembelian"{{@$data->jenis_transaksi == 'pembelian' ? 'selected' : ''}}>Pembelian</option>
								</select>
              </div>
            </div>
            <button type="submit" class="btn btn-success float-right"><i class="icon-paperplane mr-2"></i> {{$type == 'create' ? "BUAT" : "SIMPAN"}}</button>
          </form>
        </div>
			</div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('global_assets/js/form/form-submit.js') }}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/spin.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/ladda.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/components_buttons.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/form_select2.js')}}"></script>

@endsection
