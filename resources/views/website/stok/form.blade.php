@extends('website.template')
@section('content')
<div class="content-wrapper">
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">{{$type == 'create' ? 'Tambah' : 'Edit'}} Stok Barang</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <div class="content">
      <div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title"><b>Form Stok</b></h5>
				</div>
        <div class="card-body">
          <form class="kt-form" id="form-submit" data-action="{{($type == 'create') ? route('barang.stok.created',Request::segment(3)) : route('barang.stok.update',$dataStok->id)}}" enctype="multipart/form-data" autocomplete="off">
            {{csrf_field()}}
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Nama Barang</label>
              <div class="col-lg-12">
                <input type="text" name="nama_barang" placeholder="masukan nama barang" disabled value="{{$data->nama_barang}}" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Jumlah Stok</label>
              <div class="col-lg-12">
                <input type="number" name="total_barang" placeholder="masukan jumlah stok" value="{{$type == 'create' ? '' : $dataStok->total_barang}}" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-12">Total Stok</label>
              <div class="col-lg-12">
                <select name="jenis_stok" class="form-control select">
                  <option value="in" {{@$dataStok->jenis_stok == 'in' ? 'Selected' : ''}}>In</option>
                  <option value="out" {{@$dataStok->jenis_stok == 'out' ? 'Selected' : ''}}>Out</option>
              </select>
              </div>

            </div>
            <button type="submit" class="btn btn-success float-right"><i class="icon-paperplane mr-2"></i> {{$type == 'create' ? "BUAT" : "SIMPAN"}}</button>
          </form>
        </div>
			</div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('global_assets/js/form/form-submit.js') }}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/spin.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/buttons/ladda.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/components_buttons.js')}}"></script>
<script src="{{ asset('global_assets/assets/plugins/fileuploads/js/dropify.min.js') }}"></script>
<script src="{{asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('global_assets/js/demo_pages/form_select2.js')}}"></script>
@endsection
