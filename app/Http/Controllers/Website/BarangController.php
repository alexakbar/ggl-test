<?php

namespace App\Http\Controllers\Website;

use App\Entities\Barang;
use App\Entities\Stok;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Responder;
use App\Helper\UploadImage;
use Validator;

class BarangController extends Controller
{
  use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['barangs'] = Barang::all();
        $data['stoks'] = Stok::all();

        return view('website.barang.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['type'] = "create";
        return view('website.barang.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'image' => 'required',
          'nama_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $barang = new Barang();
          $barang->kode_barang = generate_unique_string_and_int(4,4);
          $barang->nama_barang = $request->nama_barang;
          if (!empty($request->file('image'))) {
              $barang->gambar_barang = UploadImage::single($request->image, 'random', Barang::$directory_image);
          }

          if ($barang->save()) {
            $response['redirect_url'] = route('index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['data'] = Barang::find($id);
      return view('website.barang.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type'] = "edit";
        $data['data'] = Barang::find($id);
        return view('website.barang.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'nama_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $barang = Barang::find($id);
          $barang->nama_barang = $request->nama_barang;
          if (!empty($request->file('image'))) {
            if (is_file_exists($barang->gambar_barang)) {
                remove_file($barang->gambar_barang);
            }
            $barang->gambar_barang = UploadImage::single($request->image, 'random', Barang::$directory_image);
          }

          if ($barang->save()) {
            $response['redirect_url'] = route('index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $barang = Barang::find($id);

        if ($barang->stok->count() == 0) {
          if (is_file_exists($barang->gambar_barang)) {
              remove_file($barang->gambar_barang);
          }
          $barang->delete();
          $response['redirect_url'] = route('index');
          return $this->response(200, $response);
        }else{
          $response['message'] = array_merge(['Barang mempunyai stok']);
          return $this->response(400, $response);
        }
    }
}
