<?php

namespace App\Http\Controllers\Website;

use App\Entities\Transaksi;
use App\Entities\Stok;
use App\Entities\Barang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Responder;
use Validator;

class TransaksiController extends Controller
{
  use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['transaksis'] = Transaksi::all();

        return view('website.transaksi.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['type'] = "create";
        $data['barangs'] = Barang::all();
        return view('website.transaksi.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'total_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $transaksi = new Transaksi();

          $stokIn = Stok::where('id_barang',$request->id_barang)->where('jenis_stok','in')->get()->sum('total_barang');
          $stokOut = Stok::where('id_barang',$request->id_barang)->where('jenis_stok','out')->get()->sum('total_barang');
          $jumlahTransaksiPembelian = Transaksi::where('id_barang',$request->id_barang)->where('jenis_transaksi','pembelian')->get()->sum('total_barang');
          $jumlahTransaksiPenjualan = Transaksi::where('id_barang',$request->id_barang)->where('jenis_transaksi','penjualan')->get()->sum('total_barang');
          $realStok = ($stokIn + $jumlahTransaksiPembelian) - ($jumlahTransaksiPenjualan + $stokOut);
          if ($request->total_barang > $realStok && $request->jenis_transaksi != "pembelian") {
            $response['message'] = array_merge(['Jumlah barang melebihi stok'],['stok saat ini '.$realStok]);
            return $this->response(400, $response);
          }else{
            $transaksi->kode_transaksi = generate_unique_string_and_int(5,5);
            $transaksi->id_barang = $request->id_barang;
            $transaksi->total_barang = $request->total_barang;
            $transaksi->jenis_transaksi = $request->jenis_transaksi;

            if ($transaksi->save()) {
              $response['redirect_url'] = route('transaksi.index');
              return $this->response(200, $response);
            }
          }

        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['data'] = Transaksi::find($id);
      return view('website.transaksi.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type'] = "edit";
        $data['data'] = Transaksi::find($id);
        $data['barangs'] = Barang::all();
        return view('website.transaksi.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'total_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $transaksi = Transaksi::find($id);
          $stokIn = Stok::where('id_barang',$request->id_barang)->where('jenis_stok','in')->get()->sum('total_barang');
          $stokOut = Stok::where('id_barang',$request->id_barang)->where('jenis_stok','out')->get()->sum('total_barang');
          $jumlahTransaksiPembelian = Transaksi::where('id_barang',$request->id_barang)->where('jenis_transaksi','pembelian')->get()->sum('total_barang');
          $jumlahTransaksiPenjualan = Transaksi::where('id_barang',$request->id_barang)->where('jenis_transaksi','penjualan')->get()->sum('total_barang');
          $realStok = ($stokIn + $jumlahTransaksiPembelian) - ($jumlahTransaksiPenjualan + $stokOut);
          if ($request->total_barang > $realStok && $request->jenis_transaksi != "pembelian") {
            $response['message'] = array_merge(['Jumlah barang melebihi stok'],['stok saat ini '.$realStok]);
            return $this->response(400, $response);
          }else{
            $transaksi->total_barang = $request->total_barang;
            $transaksi->jenis_transaksi = $request->jenis_transaksi;
            if ($transaksi->save()) {
              $response['redirect_url'] = route('transaksi.index');
              return $this->response(200, $response);
            }
          }


        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $transaksi = Transaksi::find($id);
        $transaksi->delete();
        $response['redirect_url'] = route('transaksi.index');
        return $this->response(200, $response);

    }
}
