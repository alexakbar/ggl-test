<?php

namespace App\Http\Controllers\Website;

use App\Entities\Stok;
use App\Entities\Barang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Responder;
use Validator;

class StokController extends Controller
{
  use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['type'] = "create";
        $data['data'] = Barang::find($id);
        return view('website.stok.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
          'total_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $stok = new Stok();
          $stok->total_barang = $request->total_barang;
          $stok->jenis_stok = $request->jenis_stok;
          $stok->id_barang = $id;

          if ($stok->save()) {
            $response['redirect_url'] = route('index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type'] = "edit";
        $data['dataStok'] = Stok::find($id);
        $data['data'] = $data['dataStok']->barang;
        return view('website.stok.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'total_barang' => 'required',
        ]);

        if ($validator->passes()) {
          $stok = Stok::find($id);
          $stok->total_barang = $request->total_barang;
          $stok->jenis_stok = $request->jenis_stok;

          if ($stok->save()) {
            $response['redirect_url'] = route('index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $stok = Stok::find($id);

        $stok->delete();
        $response['redirect_url'] = route('index');
        return $this->response(200, $response);
    }
}
