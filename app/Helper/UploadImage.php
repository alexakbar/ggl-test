<?php

namespace App\Helper;

use ImageOptimizer;

class UploadImage
{
    public static $file_name;

    public static function single($image, $filename, $path)
    {
        if (!empty($image)) {
            if ($filename == 'random') {
                Self::$file_name = random_filename($image);
            } else {
                Self::$file_name = $filename;
            }

            $image->move(public_path($path), Self::$file_name);

            return $path.Self::$file_name;
        }
    }
}
