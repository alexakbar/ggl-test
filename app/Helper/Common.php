<?php
if (!function_exists('generate_unique_string')) {
    function generate_unique_string($length = null, $start_string = null, $finish_string = null)
    {
        $string = '';
        if ($start_string) {
            $string .= $start_string;
        }
        if ($length) {
            $string .= str_shuffle(strtoupper(Str::random($length).rand(1, 100)));
        } else {
            $string .= str_shuffle(strtoupper(Str::random().rand(1, 100)));
        }
        if ($finish_string) {
            $string .= $finish_string;
        }

        return $string;
    }
}

if (!function_exists('is_file_exists')) {
    function is_file_exists($file)
    {
        return $file !== null && is_file($file);
    }
}

if (!function_exists('remove_file')) {
    function remove_file($path)
    {
        if (unlink($path)) {
            return true;
        }

        return false;
    }
}

if (!function_exists('filename')) {
    function filename($text)
    {
        $text = str_slug($text);
        $text = str_limit($text, 5);
        $date = str_slug(\Carbon\Carbon::now());
        $rand = Str::random(10);
        $text = str_shuffle($text.$rand.$date);
        $text = str_replace(['-','_','.'], '', $text);
        return $text;
    }
}

if (!function_exists('generate_unique_string_and_int')) {
    function generate_unique_string_and_int($lengthstring, $lengthint =null)
    {
        $string = 'ABCDEFGHIJKLMNPQRSTUV123456789';
        $strstring = substr(str_shuffle(str_repeat($string, 5)), 0, $lengthstring);

        $int = "123456789";
        $strint = substr(str_shuffle(str_repeat($int, 5)), 0, 2);

        return $strstring.$strint;
    }
}

if (!function_exists('random_filename')) {
    function random_filename($file)
    {
        return generate_unique_string().'.'.$file->getClientOriginalExtension();
    }
}

if (!function_exists('img_holder')) {
    function img_holder($type = null)
    {
        switch ($type) {
          case 'avatar':
            return asset('files/placeholder/avatar.png');
          break;
          case 'file':
            return asset('files/placeholder/fileholder.png');
          break;
          case 'image':
            return asset('files/placeholder/default.png');
          break;
          default:
            return asset('files/placeholder/300x300.png');
          break;
        }
    }
}

if (!function_exists('indonesian_month')) {
    function indonesian_month()
    {
        $month = [
          1  => 'Januari',
          2  => 'Februari',
          3  => 'Maret',
          4  => 'April',
          5  => 'Mei',
          6  => 'Juni',
          7  => 'Juli',
          8  => 'Agustus',
          9  => 'September',
          10 => 'Oktober',
          11 => 'November',
          12 => 'Desember',
        ];

        return $month;
    }
}

if (!function_exists('convertMonth')) {
    function convertMonth($currentmonth)
    {
        $month = [
          "01"  => 'January',
          "02"  => 'February',
          "03"  => 'March',
          "04"  => 'April',
          "05"  => 'May',
          "06"  => 'June',
          "07"  => 'July',
          "08"  => 'August',
          "09"  => 'September',
          "10" => 'October',
          "11" => 'November',
          "12" => 'December',
        ];

        return array_search($currentmonth,$month);
    }
}

if (!function_exists('active_route')) {
    function active_route($current)
    {
        if (Route::currentRouteName() == $current) {
            return 'active';
        }
        return false;
    }
}



if (!function_exists('rupiah')) {
    function rupiah($price)
    {
        return "Rp " . number_format($price, 0, ',', '.');
    }
}
