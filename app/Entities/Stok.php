<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    public function barang()
    {
        return $this->belongsTo('App\Entities\Barang', 'id_barang');
    }
}
