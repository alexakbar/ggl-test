<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public static $directory_image = 'files/barang/';

    public function stok()
    {
        return $this->hasMany('App\Entities\Stok','id_barang','id');
    }
}
