<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function barang()
    {
        return $this->belongsTo('App\Entities\Barang', 'id_barang');
    }
}
