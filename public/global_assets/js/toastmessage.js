String.prototype.UcFirst = function() {
	return this.charAt(0).toUpperCase() + this.substr(1);
}

function redirect(url, timer) {
	if (url.includes('datatable')) {
		$(url).DataTable().ajax.reload();
		return;
	}

	setTimeout(function() {
		if (url == 'reload()') {
			window.location.reload();
		} else {
			window.location.href = url;
		}
	}, timer)
}

function toastMessage(type, object) {
	var time = 0;

	if (type == 'undefined' || type == null) {
		return false;
	}

	if ($.isEmptyObject(object)) {
		toastr[type](type.UcFirst());
	} else {
		$.each(object, function(index, value) {
			setTimeout(function() {
				toastr[type](value);
			}, time);
			time += 500;
		});
	}

	return time;
}
