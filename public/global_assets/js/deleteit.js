function deleteIt(that) {


  Swal.fire({
      title: 'Apakah anda yakin?',
      text: "kamu akan menghapus item ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Iya, hapus',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
  }).then(function(result) {
      if(result.value) {
        var action = $(that).data('action');
        $.ajaxSetup({
              headers:
              { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
          });
          $.ajax({
              url: action,
              contentType: false,
              cache: false,
              processData: false,
              dataType: 'json',
              type: 'POST',
              success: function(response) {
                let toast = toastMessage('success', response.data.message);
                if (response.data.redirect_url) {
                    redirect(response.data.redirect_url, toast += 500);
                }else{
                  location.reload();
                }
              },
              error: function(request, status, error) {
                toastMessage('error', request.responseJSON.data.message);
              }
          });
      }
  });
}
