<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Website\BarangController@index')->name('index');
Route::group(['as' => 'barang.', 'prefix' => '/barang'], function () {
  Route::get('/', 'Website\BarangController@index')->name('index');
  Route::get('/create', 'Website\BarangController@create')->name('create');
  Route::post('/store', 'Website\BarangController@store')->name('created');
  Route::get('/show/{id}', 'Website\BarangController@show')->name('show');
  Route::get('/edit/{id}', 'Website\BarangController@edit')->name('edit');
  Route::post('/update/{id}', 'Website\BarangController@update')->name('update');
  Route::post('/destroy/{id}', 'Website\BarangController@destroy')->name('destroy');

  Route::group(['as' => 'stok.', 'prefix' => '/stok'], function () {
    Route::get('/{id}/create', 'Website\StokController@create')->name('create');
    Route::post('/{id}store', 'Website\StokController@store')->name('created');
    Route::get('/edit/{id}', 'Website\StokController@edit')->name('edit');
    Route::post('/update/{id}', 'Website\StokController@update')->name('update');
    Route::post('/destroy/{id}', 'Website\StokController@destroy')->name('destroy');
  });
});

Route::group(['as' => 'transaksi.', 'prefix' => '/transaksi'], function () {
  Route::get('/', 'Website\TransaksiController@index')->name('index');
  Route::get('/create', 'Website\TransaksiController@create')->name('create');
  Route::post('/store', 'Website\TransaksiController@store')->name('created');
  Route::get('/show/{id}', 'Website\TransaksiController@show')->name('show');
  Route::get('/edit/{id}', 'Website\TransaksiController@edit')->name('edit');
  Route::post('/update/{id}', 'Website\TransaksiController@update')->name('update');
  Route::post('/destroy/{id}', 'Website\TransaksiController@destroy')->name('destroy');
});
