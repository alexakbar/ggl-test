<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
          $table->id();
          $table->unsignedBigInteger("id_barang");
          $table->string('kode_transaksi');
          $table->integer("total_barang");
          $table->enum('jenis_transaksi', ['penjualan', 'pembelian']);
          $table->timestamps();

          $table
          ->foreign('id_barang')
          ->references('id')
          ->on('barangs')
          ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
